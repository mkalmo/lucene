import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../node_modules/bootstrap-treeview/dist/bootstrap-treeview.min.js';
import * as angular from  'angular';

import SearchCtrl from './search.ctrl';

angular.module('app', [])
    .controller('SearchCtrl', SearchCtrl);


angular.element(document)
    .ready(function () {
        angular.bootstrap(document, ['app']);
    });
