import { IHttpService } from 'angular';

export default class SearchCtrl {
    static readonly $inject = ['$http'];

    public query = '';
    public type = '';
    public message = '';

    constructor(private $http: IHttpService) {}

    search() {
        this.message = '';

        let conf = {
            showBorder: false,
            highlightSelected: false,
            levels: 10,
            data: <any> undefined,
            enableLinks: true
        };

        let url = `api/search?query=${this.query}&type=${this.type}`;

        return this.$http.get(url)
            .then(result => {
                conf.data = result.data;
                if (conf.data.length === 0) {
                    this.message = 'No results!';
                }
                let node: any = $('#tree');
                node.treeview(conf);
            }).catch(e => this.message = e);
    }
}
