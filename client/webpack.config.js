var path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {

    entry: {
        app: './app/app.ts'
    },

    output: {
        path: path.resolve(__dirname, '../src/main/webapp'),
        publicPath: '/',
        filename: 'bundle.js'
    },

    plugins: [
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
        }),

        new HtmlWebpackPlugin({
            template: './index.html',
            inject: false
        })
    ],

    module: {
        loaders: [
            {
                test: /\.ts$/,
                loader: 'awesome-typescript-loader'
            },
            {
                test: /\.css$/,
                loader: 'style-loader!css-loader'
            },
            {
                test: /\.woff($|\?)|\.woff2($|\?)|\.ttf($|\?)|\.eot($|\?)|\.svg($|\?)/,
                loader: 'url-loader'
            }
        ],
        exprContextCritical: false
    },

    resolve: {
        extensions: ['.js', '.ts']
    },

    devServer: {
        proxy: {
            '/lucene': {
                target: 'http://localhost:8080',
                secure: false
            }
        }
    }
};
