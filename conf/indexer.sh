#!/bin/bash

SCRIPT_PATH=$(test -L "$0" && readlink "$0" || echo "$0")
SCRIPT_DIR="$(dirname "$SCRIPT_PATH")"

java -jar $SCRIPT_DIR/indexer.jar $SCRIPT_DIR
