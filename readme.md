Install 

Install indexer

- Build jar: mvn package
- Copy target/indexer.jar and /conf/* to some location
- run indexer.sh

Install web interface

- Set index location in conf/conf.properties
- Modify server_conf.xml
- Build and deploy web application
  $(cd client && npm run build)
  mvn clean package tomcat7:redeploy -s server_conf.xml
