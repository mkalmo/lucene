package common;

import java.io.File;
import java.util.List;
import java.util.Properties;

public class Conf {

    private List<File> locations;
    private Properties properties;
    private List<File> additionalFiles;

    public Conf(List<File> locations, List<File> deletedFiles, Properties properties) {
        this.locations = locations;
        this.additionalFiles = deletedFiles;
        this.properties = properties;

    }

    public List<File> getLocations() {
        return locations;
    }

    public List<File> getAdditionalFiles() {
        return additionalFiles;
    }

    public File getIndexLocation() {
        return new File(properties.getProperty(Globals.INDEX_LOCATION_KEY));
    }


}
