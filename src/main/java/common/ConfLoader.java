package common;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;

public class ConfLoader {

    private File confDir;
    private Properties properties;

    public ConfLoader(File confDir) {
        this.confDir = confDir;
        this.properties = readConf();
    }

    public Conf load() {
        return new Conf(readLocations(), getAdditionalFiles(), properties);
    }

    private Properties readConf() {
        File file = new File(confDir, Globals.CONF_FILE_NAME);
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream(file.getPath()));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return properties;
    }

    protected boolean isValidDir(File dir) {
        return dir.exists() && dir.isDirectory();
    }

    private List<File> readLocations() {
        try {
            List<File> dirs = new ArrayList<>();
            File conf = new File(confDir, Globals.LOCATIONS_FILE_NAME);
            List<String> locations = Files.readAllLines(conf.toPath(), Globals.CHARSET);

            for (String location : locations) {
                File dir = new File(location);
                if (isValidDir(dir)) {
                    dirs.add(dir);
                } else {
                    System.err.println(dir + " is not a valid directory");
                }
            }

            return dirs;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private List<File> getAdditionalFiles() {
        List<File> files = new ArrayList<>();
        files.addAll(readAdditionalFilesCommon(Globals.DELETED_LIST_FILE_KEY, new File("/deleted")));
//        files.addAll(readAdditionalFilesCommon(Globals.ADDITIONAL_LIST_FILE_KEY, new File("/")));
        return files;
    }

    private List<File> readAdditionalFilesCommon(String key, File parentDir) {
        String filePath = properties.getProperty(key);
        filePath = filePath.replace("${conf_dir}", confDir.getAbsolutePath());

        Path path = new File(filePath).toPath();

        try {
            List<File> filePaths = Files.readAllLines(path, Globals.CHARSET)
                    .stream()
                    .map(File::new)
                    .collect(Collectors.toList());

            return filePaths.stream()
                    .map(fp -> new File(parentDir, fp.getPath()))
                    .collect(Collectors.toList());

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }




}
