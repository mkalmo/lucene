package common;

import java.util.*;
import java.util.stream.Collectors;

public class FileNode {

    public enum Type { FILE, DIR }

    private final Type type;
    private final String name;
    private final FileNode parent;
    private final List<FileNode> children = new ArrayList<>();

    private FileNode(String name, FileNode parent, Type type) {
        this.name = name;
        this.parent = parent;
        this.type = type;
    }

    public static FileNode createRoot() {
        return new FileNode("", null, FileNode.Type.DIR);
    }

    public boolean isDirectory() {
        return type == Type.DIR;
    }

    public boolean isFile() {
        return type == Type.FILE;
    }

    public List<FileNode> getChildren() {
        return Collections.unmodifiableList(children);
    }

    public String getName() {
        return name;
    }

    public FileNode createChild(String name, Type type) {
        children.stream()
                .filter(node -> node.name.equals(name))
                .findFirst()
                .ifPresent(node -> {
//                    throw new IllegalStateException("same name: " + name);
                    // with additional files its not known whether entry is dir or file
                });

        FileNode newDir = new FileNode(name, this, type);

        children.add(newDir);

        return newDir;
    }

    public List<String> getPathElements() {
        LinkedList<String> pathElements = new LinkedList<>();
        FileNode runner = this;
        while (runner != null) {
            pathElements.addFirst(runner.name);
            runner = runner.parent;
        }
        return pathElements;
    }

    public String stringify() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.name + "\n");
        for (FileNode node : children) {
            sb.append(multiply("   ", node.getPathElements().size() - 1));
            if (node.isDirectory()) {
                sb.append(node.stringify());
            } else {
                sb.append(node.name + " (f) \n");
            }
        }

        return sb.toString();
    }

    private String multiply(String what, int howManyTimes) {
        return Arrays.asList(new String[howManyTimes]).stream()
                .map(e -> what).collect(Collectors.joining());
    }

}