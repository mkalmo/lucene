package common;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

public class FileNodeTree {

    private final FileNode root = FileNode.createRoot();

    public FileNode getRoot() {
        return root;
    }

    public FileNodeTree addFile(String subPath) {
        FileNode parentDir = getOrCreateSubDirs(Paths.get(subPath).getParent());

        String name = Paths.get(subPath).getFileName().toString();

        parentDir.createChild(name, FileNode.Type.FILE);

        return this;
    }

    public FileNodeTree addDirectory(String subPath) {
        getOrCreateSubDirs(Paths.get(subPath));

        return this;
    }

    private FileNode getOrCreateSubDirs(Path subPath) {
        FileNode runner = root;

        for (int i = 0; i < subPath.getNameCount(); i++) {
            String newName = subPath.getName(i).toString();

            Optional<FileNode> optional = getChildDir(runner, newName);

            if (optional.isPresent()) {
                runner = optional.get();
            } else {
                runner = runner.createChild(newName, FileNode.Type.DIR);
            }
        }

        return runner;
    }

    private Optional<FileNode> getChildDir(FileNode node, String dirName) {
        return node.getChildren().stream()
                .filter(child -> child.getName().equals(dirName))
                .findFirst();
    }


}
