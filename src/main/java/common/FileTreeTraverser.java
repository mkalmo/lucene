package common;

import java.io.File;
import java.util.*;

import static java.util.Arrays.*;

public class FileTreeTraverser {

    private List<File> files = new ArrayList<>();

    public FileTreeTraverser(List<File> dirs) {
        files.addAll(dirs);
    }

    public Iterator<File> iterator() {
        return new FileIterator(files);
    }

    private class FileIterator implements Iterator<File> {
        private LinkedList<File> iteratorFiles;

        public FileIterator(List<File> files) {
            this.iteratorFiles = new LinkedList<>(files);
        }

        @Override
        public boolean hasNext() {
            return iteratorFiles.size() > 0;
        }

        @Override
        public File next() {
            if (!hasNext()) {
                throw new IllegalStateException("no more elements");
            }

            File current = iteratorFiles.removeFirst();

            if (current.isDirectory()) {
                iteratorFiles.addAll(asList(current.listFiles()));
            }

            return current;
        }
    }

}
