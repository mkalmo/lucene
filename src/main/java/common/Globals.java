package common;

import java.nio.charset.Charset;

public class Globals {

    public static final Charset CHARSET = Charset.forName("UTF-8");

    public static final String LOCATIONS_FILE_NAME = "locations.txt";
    public static final String CONF_FILE_NAME = "conf.properties";

    public static final String DELETED_LIST_FILE_KEY = "deleted_list_file";
    public static final String ADDITIONAL_LIST_FILE_KEY = "additional_list_file";
    public static final String INDEX_LOCATION_KEY = "index_location";

}
