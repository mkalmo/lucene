package common;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.*;
import org.apache.lucene.index.*;
import org.apache.lucene.store.*;
import org.apache.lucene.util.Version;

public class IndexBuilder {

    private final StandardAnalyzer analyzer = new StandardAnalyzer(
            Version.LUCENE_45);
    private final Directory index;

    public IndexBuilder(File indexPath) {
        index = getIndex(indexPath);
    }

    public void buildIndex(Iterator<File> fileIterator) {
        IndexWriterConfig config = new IndexWriterConfig(
                Version.LUCENE_45, analyzer);
        config.setOpenMode(IndexWriterConfig.OpenMode.CREATE);

        try {
            IndexWriter indexWriter = new IndexWriter(index, config);

            while (fileIterator.hasNext()) {
                addDoc(indexWriter, fileIterator.next());
            }

            indexWriter.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static void addDoc(IndexWriter w, File file)
            throws IOException {

        String name = file.getName().replaceAll("[_.',\\-+]", " ");

        Document doc = new Document();
        doc.add(new TextField("name", name, Field.Store.YES));
        String type = file.isDirectory() ? "D" : "F";
        doc.add(new TextField("type", type, Field.Store.YES));
        // use a string field for path because we don't want it tokenized
        doc.add(new StringField("path", file.getAbsolutePath(), Field.Store.YES));

        w.addDocument(doc);
    }

    private static Directory getIndex(File path) {
        try {
            return FSDirectory.open(path);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
