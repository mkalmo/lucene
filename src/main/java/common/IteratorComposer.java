package common;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

public class IteratorComposer<T> implements Iterator<T> {

    private List<Iterator<T>> iterators = new ArrayList<>();

    public IteratorComposer<T> addIterator(Iterator<T> iterator) {
        iterators.add(iterator);

        return this;
    }

    @Override
    public boolean hasNext() {
        return getNextIterator().isPresent();
    }

    @Override
    public T next() {
        return getNextIterator()
                .orElseThrow(() -> new IllegalStateException("no more elements"))
                .next();
    }

    private Optional<Iterator<T>> getNextIterator() {
        for (Iterator<T> it: iterators) {
            if (it.hasNext()) {
                return Optional.of(it);
            }
        }

        return Optional.empty();
    }
}
