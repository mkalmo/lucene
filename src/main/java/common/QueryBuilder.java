package common;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

public class QueryBuilder {

    public boolean isValidQuery(String string) {
        return StringUtils.isNotBlank(string);
    }

    public String createQuery(String string) {
        if (StringUtils.isBlank(string)) {
            throw new IllegalArgumentException("is blank");
        }

        List<String> terms = new ArrayList<>();
        for (String word : string.split("\\s+")) {
            terms.add("name:" + word);
        }

        return StringUtils.join(terms, " AND ");
    }

    public String createQuery(String string, Options options) {
        String query = createQuery(string);

        if (StringUtils.isNotBlank(options.getType())) {
            query += " AND type:" + options.getType();
        }

        return query;
    }

}
