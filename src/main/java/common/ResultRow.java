package common;

import lombok.Data;

@Data
public class ResultRow {

    private String name;

    private String path;

    private String type;
}
