package common;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.*;
import org.apache.lucene.index.*;
import org.apache.lucene.queryparser.classic.*;
import org.apache.lucene.search.*;
import org.apache.lucene.store.*;
import org.apache.lucene.util.Version;

import java.io.*;
import java.util.*;

public class Searcher {

    private final StandardAnalyzer analyzer = new StandardAnalyzer(
            Version.LUCENE_45);
    private final Directory index;

    public Searcher(File indexPath) {
        index = getIndex(indexPath);
    }

    public List<File> search(String query) {
        try {
            return searchSub(query);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private List<File> searchSub(String queryString) throws Exception {

        // the "name" arg specifies the default field to use
        // when no field is explicitly specified in the query.

        Query q = new QueryParser(Version.LUCENE_45, "name", analyzer)
                .parse(queryString);

        // 3. search
        int hitsPerPage = 200;
        IndexReader reader = DirectoryReader.open(index);
        IndexSearcher searcher = new IndexSearcher(reader);
        TopScoreDocCollector collector = TopScoreDocCollector.create(
                hitsPerPage, true);
        searcher.search(q, collector);
        ScoreDoc[] hits = collector.topDocs().scoreDocs;

        // 4. display results
        List<File> results = new ArrayList<>();
        for (ScoreDoc hit : hits) {
            int docId = hit.doc;
            Document d = searcher.doc(docId);

            results.add(new File(d.get("path")));
        }

        reader.close();

        return results;
    }

    private static Directory getIndex(File path) {
        try {
            return FSDirectory.open(path);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
