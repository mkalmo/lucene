package indexer;

import java.io.File;
import java.text.MessageFormat;
import java.util.Iterator;

import org.apache.commons.lang3.StringUtils;

import common.*;

public class Indexer {

    public static void main(String[] args) {
        Conf conf = new ConfLoader(getConfDir(args)).load();

        new Indexer().buildIndex(conf);
    }

    public void buildIndex(Conf conf) {

        IteratorComposer<File> fi = new IteratorComposer<>();
        Iterator<File> it1 = new FileTreeTraverser(conf.getLocations()).iterator();
        fi.addIterator(it1);
        Iterator<File> it2 = conf.getAdditionalFiles().iterator();
        fi.addIterator(it2);


        IteratorComposer<File> fileIterator = new IteratorComposer<File>()
                .addIterator(new FileTreeTraverser(conf.getLocations()).iterator())
                .addIterator(conf.getAdditionalFiles().iterator());

        new IndexBuilder(conf.getIndexLocation()).buildIndex(fileIterator);

        String message = MessageFormat.format(
                "locations: \n{0}\ndeleted: \n{1}\n ... Indexed ...",
                StringUtils.join(conf.getLocations(), "\n"),
                StringUtils.join(conf.getAdditionalFiles(), "\n"));

        System.out.println(message);
    }

    private static File getConfDir(String[] args) {

        if (args.length < 1) {
            exit("Provide configuration directory as an argument");
        }

        File confDir = new File(args[0]);

        if (!confDir.exists()) {
            exit(confDir + " does not exist");
        } else if (!confDir.isDirectory()) {
            exit(confDir + " is not dir");
        }

        return confDir;
    }

    public static void exit(String message) {
        System.out.println(message);
        System.exit(1);
    }

}
