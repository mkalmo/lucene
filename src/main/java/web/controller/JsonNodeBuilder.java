package web.controller;

import common.FileNode;

public class JsonNodeBuilder {

    public JsonNode fromFileNodes(FileNode node) {
        if (node.isFile()) {
            return new JsonNode(node.getName());
        }

        JsonNode dir = new JsonNode(node.getName());
        dir.setHref("doublecmd://" + String.join("/", node.getPathElements()));
        for (FileNode file : node.getChildren()) {
            dir.addNode(fromFileNodes(file));
        }

        return dir;
    }

}
