package web.controller;

import java.io.*;
import java.util.*;

import javax.annotation.Resource;

import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.*;

import common.*;

@RestController
public class LuceneController {

    @Resource
    private Environment env;

    @RequestMapping("/")
    public String home() {
        return "redirect:search";
    }

    @GetMapping("search")
    public List<JsonNode> search(
            @RequestParam(value = "query", defaultValue = "") String queryString,
            @RequestParam(value = "type", defaultValue = "") String type) {

        Options options = new Options();
        options.setType(type);

        QueryBuilder qb = new QueryBuilder();
        if (!qb.isValidQuery(queryString)) {
            return Collections.emptyList();
        }

        String query = new QueryBuilder().createQuery(queryString, options);

        File index = new File(env.getProperty(Globals.INDEX_LOCATION_KEY));
        List<File> fileList = new Searcher(index).search(query);

        FileNodeTree tree = new FileNodeTree();
        for (File file : fileList) {
            tree.addFile(file.getPath());
        }

        return new JsonNodeBuilder().fromFileNodes(tree.getRoot()).getNodes();
    }
}