package common;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Test;

public class ConfLoaderTest {

    @Test
    public void shouldLoadConfFromFiles() {

        String dir = getClass().getResource("/").getFile();

        Conf conf = new ConfLoader(new File(dir)) {
            protected boolean isValidDir(File dir) { return true; }
        }.load();

        assertThat(asString(conf.getLocations()), contains("/dir1", "/dir2"));

        assertThat(conf.getIndexLocation().getName(), is("lucene_index"));

        assertThat(asString(conf.getAdditionalFiles()),
                contains("/deleted/deleted1",
                        "/deleted/deleted2"));
    }

    private List<String> asString(List<File> locations) {
        return locations.stream()
                .map(f -> f.getPath())
                .collect(Collectors.toList());
    }

}
