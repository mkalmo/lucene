package common;

import org.junit.Ignore;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class FileNodeTreeTest {

    @Test
    public void buildFileNodeTree() {

        FileNodeTree tree = new FileNodeTree()
                .addFile("/a/f1")
                .addFile("/a/f2")
                .addDirectory("/a")
                .addDirectory("/b/c");

        FileNode a = tree.getRoot().getChildren().get(0);
        FileNode b = tree.getRoot().getChildren().get(1);
        FileNode f2 = a.getChildren().get(1);
        FileNode c = b.getChildren().get(0);

        assertThat(a.getName(), is("a"));
        assertTrue(a.isDirectory());
        assertThat(a.getChildren().size(), is(2));

        assertThat(b.getName(), is("b"));
        assertTrue(b.isDirectory());
        assertThat(b.getChildren().size(), is(1));

        assertThat(f2.getName(), is("f2"));
        assertTrue(f2.isFile());

        assertTrue(c.isDirectory());
    }

    @Ignore
    @Test(expected = IllegalStateException.class)
    public void sameNameThrows() {
        new FileNodeTree().addFile("/a/f1").addFile("/a");
    }

}

