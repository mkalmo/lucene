package common;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.io.File;
import java.util.*;

import org.junit.*;
import util.TestableFileBuilder;

public class FileTreeTraverserTest {

    @Test
    public void traversesFileTree() throws Exception {

        FileNodeTree tree = new FileNodeTree()
            .addFile("a/f1")
            .addFile("a/f2")
            .addFile("b/f3");

        File rootFile = new TestableFileBuilder().build(tree.getRoot());

        Iterator<File> it = new FileTreeTraverser(
                Arrays.asList(rootFile.listFiles())).iterator();

        assertThat(it.next().getName(), is("a"));
        assertThat(it.next().getName(), is("b"));
        assertThat(it.next().getName(), is("f1"));
        assertThat(it.next().getName(), is("f2"));
        assertThat(it.next().getName(), is("f3"));
        assertThat(it.hasNext(), is(false));
    }

}

