package common;

import org.junit.Test;

import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class IteratorComposerTest {

    @Test
    public void combinesIterators() {

        IteratorComposer<String> iterator = new IteratorComposer<String>()
                .addIterator(asList("f1").iterator())
                .addIterator(asList("f2").iterator());

        assertThat(iterator.next(), is("f1"));
        assertThat(iterator.next(), is("f2"));
        assertThat(iterator.hasNext(), is(false));
    }

    @Test(expected = IllegalStateException.class)
    public void throwsWhenNoMoreElements() {
        new IteratorComposer().next();
    }

}

