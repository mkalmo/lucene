package common;

import org.junit.Test;
import web.controller.JsonNodeBuilder;
import web.controller.JsonNode;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class JsonNodeBuilderTest {

    @Test
    public void buildJsonNodeTree() {

        List<File> results = new ArrayList<>();
        results.add(new File("/a/f1"));
        results.add(new File("/a/f2"));

        FileNodeTree tree = new FileNodeTree();
        for (File file : results) {
            tree.addFile(file.getPath());
        }

        JsonNode jsonRoot = new JsonNodeBuilder().fromFileNodes(tree.getRoot());

        JsonNode a = jsonRoot.getNodes().get(0);
        JsonNode f2 = a.getNodes().get(1);

        assertThat(a.getText(), is("a"));
        assertThat(f2.getText(), is("f2"));
    }

}

