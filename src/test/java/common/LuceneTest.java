package common;

import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.io.File;
import java.util.*;

import org.junit.*;
import org.junit.rules.TemporaryFolder;
import util.TestableFileBuilder;

public class LuceneTest {

    @Rule
    public TemporaryFolder tmpFolder = new TemporaryFolder();

    public File index;

    @Before
    public void buildIndex() throws Exception {

        FileNodeTree tree = new FileNodeTree()
            .addFile("dir1/perl")
            .addFile("dir1/java-program")
            .addFile("dir2/java-lib");

        File rootFile = new TestableFileBuilder().build(tree.getRoot());

        index = tmpFolder.newFolder("lucene_index");

        IteratorComposer<File> fileIterator = new IteratorComposer<File>()
                .addIterator(new FileTreeTraverser(asList(rootFile.listFiles())).iterator())
                .addIterator(asList(new File("deleted/extra")).iterator());

        new IndexBuilder(index).buildIndex(fileIterator);
    }

    @Test
    public void shouldFindSingleWord() {
        List<File> results = new Searcher(index).search("perl");

        assertThat(results.size(), is(1));
        assertThat(results.get(0).getName(), containsString("perl"));
    }

    @Test
    public void shouldFindExtraFiles() {
        List<File> results = new Searcher(index).search("extra");

        assertThat(results.size(), is(1));
        assertThat(results.get(0).getName(), containsString("extra"));
    }

    @Test
    public void shouldFindByMultipleWords() {
        List<File> results = new Searcher(index)
            .search(new QueryBuilder().createQuery("java lib"));

        assertThat(results.size(), is(1));
        assertThat(results.get(0).getName(), containsString("java-lib"));
    }

}
