package common;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import org.junit.Test;

public class QueryBuilderTest {

    @Test
    public void shouldConvertSpacesToAND() {
        String q = new QueryBuilder().createQuery("lib java");
        assertThat(q, is("name:lib AND name:java"));
    }

    @Test
    public void shouldSupportOptions() {
        Options options = new Options();
        options.setType("D");
        String qs = new QueryBuilder().createQuery("a", options);
        assertThat(qs, is("name:a AND type:D"));
    }


}
