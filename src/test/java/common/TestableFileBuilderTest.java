package common;

import org.junit.Test;
import util.TestableFileBuilder;

import java.io.File;
import java.util.List;

import static java.util.Arrays.*;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class TestableFileBuilderTest {

    @Test
    public void buildFileNodeTree() {

        FileNodeTree tree = new FileNodeTree()
                .addFile("/a/f1")
                .addFile("/a/f2")
                .addDirectory("/b/c");

        File root = new TestableFileBuilder().build(tree.getRoot());

        File a = root.listFiles()[0];
        File b = root.listFiles()[1];
        File f2 = a.listFiles()[1];
        File c = b.listFiles()[0];

        assertThat(a.getName(), is("a"));
        assertTrue(a.isDirectory());

        assertThat(b.getName(), is("b"));
        assertTrue(b.isDirectory());

        assertThat(f2.getName(), is("f2"));
        assertThat(f2.getPath(), is("/a/f2"));
        assertTrue(f2.isFile());

        assertTrue(c.isDirectory());
    }
}

