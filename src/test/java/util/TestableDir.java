package util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class TestableDir extends File {

    private static final long serialVersionUID = 1L;

    private List<File> files = new ArrayList<>();

    public TestableDir(String path) {
        super(path);
    }

    @Override
    public boolean isDirectory() {
        return true;
    }

    @Override
    public boolean isFile() {
        return false;
    }

    @Override
    public File[] listFiles() {
        return files.toArray(new File[0]);
    }

    public void addChild(File file) {
        files.add(file);
    }
}