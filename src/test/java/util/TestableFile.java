package util;

import java.io.File;

public class TestableFile extends File {

    private static final long serialVersionUID = 1L;

    public TestableFile(String path) {
        super(path);
    }

    @Override
    public boolean isDirectory() {
        return false;
    }

    @Override
    public boolean isFile() {
        return true;
    }

    @Override
    public File[] listFiles() {
        throw new IllegalStateException("it's not a directory");
    }

}