package util;

import common.FileNode;

import java.io.File;
import java.util.stream.Collectors;

public class TestableFileBuilder {

    public File build(FileNode node) {
        String path = node.getPathElements().stream()
                .collect(Collectors.joining("/"));

        if (!node.isDirectory()) {
            return new TestableFile(path);
        }

        TestableDir dir = new TestableDir(path);

        for (FileNode child : node.getChildren()) {
            dir.addChild(build(child));
        }

        return dir;
    }
}
